# TermiC
A fork of TermiC which is hosted on github https://github.com/joalricha869/TermiC
# Compilation 
To compile TermiC you must have a type of c compiler for these instruction I will assume your on Linux and have GCC installed<br>
git clone https://codeberg.org/theclosedbitter/TermiC.git
<br>
cd TermiC
<br>
gcc TermiC.c
<br>
mv a.out TermiC
<br>
./TermiC
# Installation 
If all you want to do is install TermiC, go to releases and install for your platform. 

