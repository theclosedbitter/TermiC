#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <time.h>
#include "functions.h"

typedef struct {
    char *name;
    void (*func)();
} Command;

Command commands[] = {
    {"read", read},
    {"write", write},
    {"append", append},
    {"clr", clear},
    {"help", help},
    {"dt", dt},
    {"stscr", startscreen},
    {"remove", removes},
    {"rename", renames},
    {"edit", edit},
    {"cdir", cdir},
    {"copy", copy},
    {"cknow", cknow},
    {"lknow", lknow},
    {"ccount", ccount},
    {"gcd", gcd},
    {"lcm", lcm},
    {"tconv", tconv},
    {"green", colorGreen},
    {"cmd", cmd},
    {"bash", bash},
    {"exit", exits},
    {"list", list}, 
    {"ls", list},
    {NULL, NULL}  // End marker
};

int main(void) {
    char a[100];
    printf("______________________\n");
    printf("       Terminal         \n");
    printf("______________________\n");
    dt();
    printf("Welcome to the TermiC!\n");
    printf("Type \"help\" for list of commands!\n\n");
    while(1) {
        printf(">>> ");
        fflush(stdin);
        scanf("%s", a);
        int found = 0;
        for (Command *cmd = commands; cmd->name != NULL; cmd++) {
            if (strcmp(a, cmd->name) == 0) {
                cmd->func();
                found = 1;
                break;
            }
        }
        if (!found) {
            printf("Enter only stated things in help\n");
        }
    }
    return 0;
}